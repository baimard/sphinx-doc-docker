# docker build . -t juxo/sphinx
FROM sphinxdoc/sphinx

RUN apt update ; apt install -y latexmk texlive-latex-extra zip git

RUN /usr/local/bin/python -m pip install --upgrade pip

WORKDIR /docs
ADD requirements.txt /docs
RUN pip3 install -r requirements.txt
RUN pip3 install git+https://github.com/sphinx-contrib/video.git